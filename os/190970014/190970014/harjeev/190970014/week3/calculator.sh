#!/bin/bash
echo "Simple calculator"
echo "enter first number"
read x
echo "enter second number"
read y
check="y"
while [ $check = "y" ]
do
	echo "1.Addition"
	echo "2.Subtraction"
	echo "3.Multiplication"
	echo "4.Division"
 	echo "Enter choice"
	read choice
	case $choice in
		 1)
			echo "Addition"
			echo `expr $x + $y`
			;;
		 2)
		 	echo "Subtraction"
			echo `expr $x - $y`
			;;
		 3)
		 	echo "Multiplication"
			echo `expr $x \* $y`
			;;
		 4)
		 	echo "Division"
			echo `expr $x / $y`
			;;
		 *)
			echo "bad value"
			;;
	esac 
        echo "Do you want to calculate again"
	read check
	if [$check != "y"]
		then
		exit
	fi
done
