#!/bin/bash
g=$(stat -c "%a" $1) #this will return octal value of the  permissions
echo $g
while [ $g -gt 0 ] 
do
	back=`expr $g % 10` #it will get the last value of the octal number 
	args+=($back) #push the value into the array
	g=`expr $g / 10`
done
#at this  point we have all the values in the form of an array 
#args[2] is for user args[1] is for owner asnd $args[0] is for group
if [ ${args[2]} -eq 7 ]
	then
	echo "User has read and write and execute"
fi
if [ ${args[2]} -eq 6 ]
	then
	echo "User has read and write"
fi
if [ ${args[2]} -eq 4 ]
	then
	echo "User has read"
fi
if [ ${args[1]} -eq 7 ]
	then
	echo "Owner has read and write and execute "
fi
if [ ${args[1]} -eq 6 ]
	then
	echo "Owner has read and write"
fi
if [ ${args[1]} -eq 4 ]
	then
	echo "Owner has read"
fi
if [ ${args[0]} -eq 7 ]
	then
	echo "Group has read and write and execute "
fi
if [ ${args[0]} -eq 6 ]
	then
	echo "Group has read and write"
fi
if [ ${args[0]} -eq 4 ]
	then
	echo "Group has read"
fi
