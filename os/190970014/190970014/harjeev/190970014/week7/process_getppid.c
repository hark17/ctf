#include<sys/types.h>
#include<stdio.h>
#include<unistd.h>

int main()
	{
		 int n1=fork();
		 int n2=fork();
	         if(n1 > 0 && n2 > 0)
		 {
			printf("%d %d",n1,n2);
			printf("My id is %d \n",getppid());
		 }	
		 else if(n1==0 && n2 > 0)
		{
			printf("Message from first child\n");
			printf("%d %d",n1,n2);
		}
		else if(n1 > 0 && n2 ==0)
		{
			printf("Message from 2nd child\n");
			printf("%d %d", n1,n2);
			printf("%d",getppid());
		}
		else
		{
			printf("Third child\n");
		}
		return 0;	
	}

				
