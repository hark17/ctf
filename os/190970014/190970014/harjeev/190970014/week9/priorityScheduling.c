#include<sys/types.h>
#include<stdio.h>
#include<unistd.h>

int main()
	{
		int p[5][4];		
		int i=0,j=0,n=0;
		int tempBT=0,tempAT=0,sum=0,tempP=0,sumC=0,sumTAT=0;
		float awt=0;
		printf("Enter number of process\n");
		scanf("%d",&n);	
		for(i=0;i<n;i++)	
		{
			for( j=0;j<2;j++)
				{
					p[i][j]=0;
				}
		}
		for(i=0;i<n;i++)
		{
				printf("Enter process burst time\n");
				scanf("%d",&p[i][0]);
				printf("Enter priority\n");
				scanf("%d",&p[i][1]);
		}
		for(i=0;i<n;i++) 
			{
				for(j=0;j<n-1;j++)
				{
					if(p[j+1][1] < p[j][1])
					{
						 tempBT=p[j+1][0];
						 tempP=p[j+1][1];
						 p[j+1][0]=p[j][0];
						 p[j+1][1]=p[j][1];
						 p[j][0]=tempBT;
						 p[j][1]=tempP;
					}
				}	
			}
		for(j=0;j<n;j++)
			{
			
				if((p[j][1]==p[j+1][1]) && (p[j][0]>p[j+1][0]))
				{
						 tempBT=p[j+1][0];
						 tempP=p[j+1][1];
						 p[j+1][0]=p[j][0];
						 p[j+1][1]=p[j][1];
						 p[j][0]=tempBT;
						 p[j][1]=tempP;
				}
			}
		for(i=0;i<n;i++)
			{
				printf("\nBT::%dAT::%d\n\n",p[i][0],p[i][1]);	
			}	
		for(i=0;i<n;i++)
			{
				sum=sum+p[i][0];
				p[i][2]=sum;
				if(i==0)
					p[i][3]=0;
				else
					p[i][3]=p[i-1][2];
			}
		for(i=0;i<n;i++)
			{
				sumC=sumC+p[i][2];	
				sumTAT=sumTAT+p[i][3];
			}
		printf("\n\nAVERAGE WAITING TIME IS : %d\n\n",sumC/n); 
		printf("\n\nAVERAGE TURNAROUND TIME IS : %d\n\n",sumTAT/n); 
		return 0;
	}

