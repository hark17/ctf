#include<stdio.h>
void display(int p[],int alloc[],int max[],int need[],int cnt, int avail)
{
	printf("Available resorces (A B and C) are %d\n\n",avail);
	printf("Pid\tAlloc\tMax\tNeed\n");
	for(int i=0;i<cnt;i++)
		printf("p%d\t%d\t%d\t%d\n",i+1,alloc[i],max[i],need[i]);
	printf("\n");
}
int main()
{
	int count;
	printf("Enter the number of processes waiting for resources : ");
	scanf("%d",&count);

	int pid[count],alloc[count],max[count],avail,need[count];
	printf("\n---------------------------------------------------------------------\n\n");
	printf(" Assumption : Resources available 3\n\n");

	int insta;
	printf("Enter the total instance of A B C : ");
	scanf("%d",&insta);
	
	printf("Enter the available :");
	scanf("%d",&avail);

	for(int i=0;i<count;i++)
	{
		printf("\nProcess p%d\n",i+1);
		printf("----------------------------\n");
		pid[i]=i+1;
		
		ALLOC:
		printf("Enter the allocated processes (A B and C) : ");
		scanf("%d",&alloc[i]);
		
		if(alloc[i]>insta)
		{
			printf("Allocated cannot exceed the available number of instances . Please re-enter\n\n");
			goto ALLOC;
		}
		MAX:
		printf("Enter the maximum of the process (A B and C) : ");
		scanf("%d",&max[i]);
		
		if(max[i]<alloc[i])
		{
			printf("Maximum cannot be less than allocated. Please re-enter\n\n");
			goto MAX;
		}
		if((max[i]+alloc[i])>insta)
		{
			printf("Max Value cannot exceed the available instance . Please re-enter\n\n");
			goto MAX;
		}

		/* Calculating the need*/
		need[i]=max[i]-alloc[i];
	}
	display(pid,alloc,max,need,count,avail);

	printf("\nThere are %d processes\n",count);
	printf("Enter the process to which is requesting : ");
	int ch;
	scanf("%d",&ch);
	ch-=1;

	printf("Enter the resource requested (A B and C) : ");
	int req;
	scanf("%d",&req);
	
	if(req<=need[ch])
	{
		if(req<=avail)
		{

			avail=avail-req;
			alloc[ch]=alloc[ch]+req;
			need[ch]=need[ch]-req;
			printf("After allocating the request .. Need table..\n\n");
			display(pid,alloc,max,need,count,avail);
		}
		else
			printf("Request is greater than available. Cannot assign the resource\n\n");
	}
	else
		printf("Request is greater than need. Cannot assign the resource to the process..\n\n");

	return 0;
}
