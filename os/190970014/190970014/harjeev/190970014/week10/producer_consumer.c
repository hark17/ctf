#include<stdio.h>
int count;
int up(int s)
{
	s+=1;
	return s;
}
int down(int s)
{
	s-=1;
	return s;
}
int main()
{
	int empty,full;

	printf("Enter the total space available for the product : ");
	scanf("%d",&count);

	empty=count;
	full=0;
	
	int flag=1,ch;
	char item[count];
	char alpha='a';
	for(int i=0;i<count;i++)
	{
		item[i]=alpha;
		alpha++;
	}
		
	while(flag)
	{
		printf("\n--------------------------------------------------------------------\n");
		printf("1. Produce an item\n2. Consume an item\n3. See products\n4. Exit\n");
		printf("Enter your choice : ");
		scanf("%d",&ch);

		switch(ch)
		{
			case 1:
				if(full<count)
				{
					printf("Producing an item..\n");
					printf("Produced item is %c\n",item[full]);
					full=up(full);
					empty=down(empty);
					printf("Available items to consume is %d\n\n",full);
				}
				else
					printf("List full.\n\n");
				break;
			case 2:
				if(full>0)
				{
					printf("Consuming an item..\n");
					printf("Consumed item is %c\n",item[full-1]);
					empty=up(empty);
					full=down(full);
					printf("Available space to produce is %d\n\n",empty); 
				}
				else
					printf("List is empty\n\n");
				break;
			case 3:
				printf("Items in the list are..\n\n");
				for(int i=0;i<full;i++)
					printf("%c\t",item[i]);
				printf("\n\n");
				break;
			case 4:	
				flag=0;break;
			default: printf("Wrong choice \n");
				break;

		}
	}
	return 0;
}
