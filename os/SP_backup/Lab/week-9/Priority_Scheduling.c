/*
Author : Shivaprasad Bhat
Date : 01-Oct-2019
Program to simulate Priority Job Algorithm
*/

#include<stdio.h>
int main()
{
	int process_id[10],burst_time[10],priority[10],count,wait_time[10],turn_around_time[10];
	

	/*
	Assuming that all the processes arrived at the same time.
	Scheduling will be done based on the priority given to that process.
	User Input: Process Burst time, priority
	Output: Scheduling table based on the priority and average waitning time and the burst time.
	*/
	
	/*
	User input : Total number of processes in the queue.
	*/
	USER_INPUT:
		printf("Enter the number of processes in the queue : ");
		scanf("%d",&count);
		if(count<0 || count>10)
		{
			printf("Queue cannot have more than 10 processes.\n\nPlease re-enter\n");
			goto USER_INPUT;
		}
	
	/*
	Enter the process details.
	Input: Burst time, priority
	*/
	for(int i=0;i<count;i++)
	{
		printf("----- Process %d -----",i+1);
		printf("\n\nEnter the burst time : ");
		scanf("%d",&burst_time[i]);
		printf("Enter the priority : ");
		scanf("%d",&priority[i]);
		process_id[i]=i+1;
	}


	/*
	To Sort the processes based on the priority
	*/
	for(int i=0;i<count;i++)
	{
		for(int j=0;j<count;j++)
		{

			if(priority[i]<priority[j])
			{
				int tid,tbt,tp;

				tid=process_id[i];
				process_id[i]=process_id[j];
				process_id[j]=tid;

				tbt=burst_time[i];
				burst_time[i]=burst_time[j];
				burst_time[j]=tbt;

				tp=priority[i];
				priority[i]=priority[j];
				priority[j]=tp;

			}
		}
	}		
	
	/*
	Calculating the waiting time
	Initialize the first waiting time as 0;
	*/
	wait_time[0]=0;
	float total_wt=0,avg_wt=0;
	for(int i=1;i<count;i++)
	{
		wait_time[i]=wait_time[i-1]+burst_time[i-1];
        	total_wt=total_wt+wait_time[i];
	}	
    	avg_wt=total_wt/count;


    	float tat=0,avg_tat=0;
    	for(int i=0;i<count;i++)
	{
   		turn_around_time[i]=wait_time[i]+burst_time[i]; 
		tat=tat+turn_around_time[i];
	}	
	avg_tat=tat/count;	
    
    /*
    To dosplay the values
    */
    
    	printf("\n\nP.ID\tB.T\tPriority\tW.T\tT.A.T\n");
	printf("--------------------------------------------------------------------------------------\n");
	for(int i=0;i<count;i++)
	{
		printf("%d\t%d\t%d\t\t%d\t%d\n",process_id[i],burst_time[i],priority[i],wait_time[i],turn_around_time[i]);
	}
   	
	printf("\n\nAverage waiting time for a process is %.2f\n",avg_wt);
	printf("\nAverage Turnaround Time is %.2f\n\n",avg_tat);
	
		

	return 0;
}
