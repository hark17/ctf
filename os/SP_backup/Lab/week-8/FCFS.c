/*Program to simulate FCFS algorithm
Author : Shivaprasad Bhat
Date: 23-Sep-19*/
#include<stdio.h>
int main()
{
	int processId[10],burstTime[10],waitingTime[10],awt,turnAroundTime[10];
	printf("\n\nSimulation of FCFS scheduling algorithm\n");
	printf("==> Assumption - Every process arrived at same time i.e time unit 0\n\n");

	int processCount;
	LBL:
	printf("Enter the total number of processes in the queue : ");
	scanf("%d",&processCount);

	if(processCount>10)
	{
		printf("Maximum number of process allowd into processor is 10\n\n");
		goto LBL;

	}
	
	/* Reading the burst time for each process*/
	for(int i=0;i<processCount;i++)
	{
		printf("Process %d\n",i+1);
		printf("Setting process id..\n");
		processId[i]=i+1;
		printf("Enter the burst time: ");
		scanf("%d",&burstTime[i]);
		printf("------------------------------------\n");
	}
	
	/*
	Assign waiting time for the first process as  0
	*/
	printf("==> Calculating waiting time\n");
	waitingTime[0]=0;
	

	/* Calculate the waiting time for remaining processes*/
	for(int i=1;i<processCount;i++)
	{
		waitingTime[i]=waitingTime[i-1]+burstTime[i-1];	
	}
	
	/* Calculating the average waiting time by adding all the wait times*/
	int totalWaitTime=0;
	for(int i=0;i<processCount;i++)
	{
		totalWaitTime=totalWaitTime+waitingTime[i];
	}
	awt=totalWaitTime/processCount;

	/* Turn around time calcultion using formula Waiting time + Burst time*/
	for(int i=0;i<processCount;i++)
	{
		turnAroundTime[i]=waitingTime[i]+burstTime[i];	
	}

	/* Calculate the average turnaround time by adding all the tat*/
	int tat=0;
	for(int i=0;i<processCount;i++)
	{
		tat=tat+turnAroundTime[i];
	}
	int atat=tat/processCount;

	/* Printing all the processes and wait time and burst time */
	printf("PID\tB.T\tW.T\tT.A.T\n");
	printf("-------------------------------\n");
	for(int i=0;i<processCount;i++)
	{
		printf("%d\t%d\t%d\t%d\n",processId[i],burstTime[i],waitingTime[i],turnAroundTime[i]);
	}
	printf("\n\nAverage waiting time of each process in this queue is : %d units\n",awt);
	printf("Average turn around time of each process in this queue is : %d units\n\n",atat);

	return 0;
}
