#include <stdio.h>
int main()
{
    int len;
    printf("Enter the length of referance string : ");
    scanf("%d", &len);

    int f;
    printf("Enter the number of frames : ");
    scanf("%d", &f);

    int ref[len];
    printf("Enter referance string ..\n");
    for (int i = 0; i < len; i++)
    {
        scanf("%d", &ref[i]);
    }

    int frame[f];
    int fcounter[f];
    for (int k = 0; k < f; k++)
    {
        frame[k] = -1;
        fcounter[k] = 0;
    }

    /*
    Initialization of first elements till frame numbers
    */
    for (int i = 0; i < f; i++)
    {
        frame[i] = ref[i];
        for (int j = 0; j < f; j++)
        {
            if (frame[j] >= 0)
            {
                printf("%d  ", frame[j]);
            }
            else
            {
                printf("  ");
            }
        }
        printf("\n");
    }

    int i = f;
    int hit = 0;
    while (i < len)
    {
        int flag = 1;
        for (int k = 0; k < f; k++)
        {
            if (ref[i] == frame[k])
            {
                flag = 0;
                hit++;
                break;
            }
        }

        if (flag == 1)
        {
            for (int k = 0; k < f; k++)
            {
                fcounter[k] = 0;
            }
            int x = i + 1;
            for (int a = 0; a < f; a++)
            {
                for (int b = x; b < len; b++)
                {
                    if (frame[a] != ref[b])
                    {
                        fcounter[a]++;
                    }
                    else
                    {
                        break;
                    }
                }
            }

            int large;

            large = 0;
            for (int i = 1; i < f; i++)
            {
                if (fcounter[i] >= fcounter[large])
                {
                    large = i;
                }
            }

            frame[large] = ref[i];
        }

        for (int k = 0; k < f; k++) 
        {
            if (frame[k] >= 0)
                printf("%d  ", frame[k]);
            else
                printf("  ");
        }
        printf("\n");

        i++;
    }

    printf("\nNumber of page hits = %d\n", hit);
    printf("\nNumber of page faults = %d\n", (len - hit));

    return 0;
}