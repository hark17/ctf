#include <stdio.h>

int main()
{
    int f;
    printf("Enter the number of frames : ");
    scanf("%d", &f);

    int frame[f];
    for (int k = 0; k < f; k++)
        frame[k] = -1;

    int len;
    printf("Enter the length of referance string : ");
    scanf("%d", &len);

    int ref[len];
    printf("Enter referance string ..\n");
    for (int i = 0; i < len; i++)
    {
        scanf("%d", &ref[i]);
    }

    printf("\n");
    int i = 0;
    int ptr = 0, hit = 0;

    for (int i = 0; i < f; i++)
        printf("f%d ", i + 1);
    printf("\n");

    while (i < len)
    {
        int flag = 1;
        for (int k = 0; k < f; k++)
        {
            if (ref[i] == frame[k])
            {
                flag = 0;
                hit++;
                break;
            }
        }

        if (flag == 1)
        {
            frame[ptr] = ref[i];
            if (ptr + 1 > f - 1)
            {
                ptr = 0;
            }
            else
            {
                ptr++;
            }
        }

        for (int k = 0; k < f; k++)
        {
            if (frame[k] >= 0)
                printf("%d  ", frame[k]);
            else
                printf("  ");
        }
        printf("\n");

        i++;
    }
    printf("\nNumber of page hits = %d\n", hit);
    printf("\nNumber of page faults = %d\n", (len - hit));
    return 0;
}