#include<stdio.h>
#include<unistd.h>
int main()
{
	int array[10];
	printf("Enter 10 elements into array\n");
	for(int i=0;i<10;i++)
	{
		printf("Enter the element %d :",i+1);
		scanf("%d",&array[i]);
	}
	int process=fork();
	if(process!=0)
	{
		printf("Parent process is displaying unsorted array[]\n");
		for(int i=0;i<10;i++)
			printf("Element %d is %d\n",i+1,array[i]);
	}

	else if(process==0)
	{
		printf("Child process is displaying sorted array[]\n");
		for(int i=0;i<10;i++)
			for(int j=0;j<10;j++)
				{
					int t;
					if(array[i]<array[j])
					{
						t=array[i];
						array[i]=array[j];
						array[j]=t;

					}
				}
		for(int i=0;i<10;i++)
			printf("Element %d is %d\n",i+1,array[i]);
	}

	return 0;

}
