#include<stdio.h>
#include<unistd.h>
int main()
{
	int n1=fork();
	int n2=fork();
	if(n1>0&&n2>0)
	{
		printf("Parent..\n");
		printf("Child ids are %d,%d\n",n1,n2);
	}
	else if(n1==0&&n2>0)
	{
		printf("n1 is parent, n2 is child\n");
		printf("Parent id is %d\n",n1);
		printf("Child id is %d\n",n2);
	
	}
	else if(n1>0&&n2==0)
	{
		printf("n2 is parent\n");
		printf("Parent id is %d\n",n2);
		printf("Child id is %d\n",n1);

	}
	else
		printf("3rd child\n");
	return 0;
}

