#include<stdio.h>
#include<unistd.h>
int main()
{
	int a[10];
	printf("Enter 10 array elements\n");
	for(int i=0;i<10;i++)
	{
		printf("Enter the number %d:",i+1);
		scanf("%d",&a[i]);
	}
	int process=fork();
	if(process==0)
	{
		int sum;
		sum=0;
		for(int i=0;i<10;i++)
		{
			if(a[i]%2==0)
				sum=sum+a[i];
		}
		printf("Child process is executing.. Calculating sum of even numbers..\n");
		printf("Sum of all the even numbers in entered array is %d\n",sum);
	}
	if(process!=0)
	{
		int sum;
		sum=0;
		for(int i=0;i<10;i++)
		{
			if(a[i]%2!=0)
				sum=sum+a[i];
		}
		printf("Parent process is executing.. Calculating sum of odd numbers..\n");
		printf("Sum of all the odd numbers in entered array is %d\n",sum);
	}
	
	return 0;
}

