#include <stdio.h>
#include <unistd.h>
int n, i;
void display(int avail[][i], int max[][i], int need[][i])
{
    for (int j = 0; j < n; j++)
    {
        for (int k = 0; k < i; k++)
        {
            printf("%d ", avail[j][k]);
        }
        printf("\t");
        for (int k = 0; k < i; k++)
        {
            printf("%d ", max[j][k]);
        }
        printf("\t");
        for (int k = 0; k < i; k++)
        {
            printf("%d ", need[j][k]);
        }
        printf("\n");
    }
    printf("\n");
}
int main()
{
    printf("Enter the number of processes : ");
    scanf("%d", &n);
    printf("Enter the number of instances :");
    scanf("%d", &i);

    //Array for available,max,allocation,need
    int avail[i], max[n][i], need[n][i], alloc[n][i];

    printf("Enter the available resources for all instances : ");
    for (int y = 0; y < i; y++)
        scanf("%d", &avail[y]);

    printf("----- Reading the data -----\n\n");
    for (int j = 0; j < n; j++)
    {
        printf("-----Process %d -----\n", j + 1);
        printf("MAX values for instances : \n");
        for (int k = 0; k < i; k++)
        {
            scanf("%d", &max[j][k]);
        }
    }

READ:
    for (int j = 0; j < n; j++)
    {
        printf("-----Process %d -----\n", j + 1);
        printf("ALLOCATION values for instances : \n");
        for (int k = 0; k < i; k++)
        {
            scanf("%d", &alloc[j][k]);
        }
    }

    for (int j = 0; j < n; j++)
        for (int k = 0; k < i; k++)
        {
            if (max[j][k] < alloc[j][k])
            {
                printf("Allocated should not exceed max limit..\n\n");
                goto READ;
            }
            else
            {
                need[j][k] = max[j][k] - alloc[j][k];
            }
        }

    printf("----------------------------------------------------------------------\n\n");
    
    //printing matrix values before the calculation
    printf("Alloc\tMax\tNeed\n");
    display(alloc, max, need);
    
    printf("Currently Available resource is : ");
    for (int x = 0; x < i; x++)
        printf("%d ", avail[x]);
    printf("\n----------------------------------------------------------------------\n\n");

    //requeting for resource
    printf("Request time..\n\n");
    printf("Processes available are :\n");
    for (int i = 0; i < n; i++)
        printf("P%d\t", i);
    printf("\n");
    printf("Please select process process number : ");
    int pc;
    scanf("%d", &pc);

    int req[i];
    printf("Enter the request for process : ");
    for (int k = 0; k < i; k++)
        scanf("%d", &req[k]);

    int flag = 1;
    int x = 0;
    //check if request can be granted or not
    while (flag == 1 && x < i)
    {
        if (req[x] <= need[pc][x])
        {
            if (req[x] > avail[x])
            {
                flag = 0;
                break;
            }
        }
        else
        {
            flag = 0;
            break;
        }
        x++;
    }

    //if request granted calculate new matrix values
    if (flag == 1)
    {
        x = 0;
        while (x < i)
        {
            avail[x] = avail[x] - req[x];
            alloc[pc][x] = alloc[pc][x] + req[x];
            need[pc][x] = need[pc][x] - req[x];
            x++;
        }
        printf("Resource is allocated for the process..\n\n");
    }
    //if request not granted
    else
    {
        printf("Resource cannot be allocated  for process %d now.\n\n", pc);
    }

    printf("----------------------------------------------------------------------\n\n");
    //printing matrix values
    printf("Alloc\tMax\tNeed\n");
    display(alloc, max, need);

    //printing avai value
    printf("Currently Available resource is : ");
    for (x = 0; x < i; x++)
        printf("%d ", avail[x]);
    printf("\n");
    

    //finding safe state
    
    int w[i], finish[n], c1=0;
    for(int a=0; a<n; a++)
    {
        finish[a]=0;
    }
        
    for(int a=0; a<i; a++)
    {
        w[a]=avail[a];
    }

    int j=0; int f=1;
    while(1)
    {
    	flag=1;
        for(int a=0; a<i; a++)
        {
            if(need[j][a]>w[a])
			{
				flag=0;
				break;
			}
        }
        
	
        if(flag==1 && finish[j]==0)
        {
        	for(int a=0;a<i;a++)
        	{
        		w[a]=w[a]+alloc[j][a];
        	}
        	
            finish[j]=1;
            printf("\np%d\t",j);
        }
        

        j++;
        
        if(j>=n)
        {
        	j=0;
        }
        
        for(int x=0;x<n;x++)
        {
        	if(finish[x]==0)
        	{
        		f=0;
        		break;
        	}
        	else
        	{
        		f=1;
        	}
        }
        
        if(f==1)
        	break;
        
        
    }
   

    return 0;
}
