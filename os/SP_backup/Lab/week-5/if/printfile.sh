#!/bin/bash
#Program to check a file exist and readable. If readable, print first two lines of the file
file=$1
echo file is $file

if [ -e $file ]
then
	if [ -r $file ]
	then
		head -2 $file
	else
		echo 'File not readable'
	fi

else
	echo 'File not present'
fi
