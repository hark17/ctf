#!/bin/bash
echo 'File operator -d'

path='/home/mca/shivaprasad/week-5/script_test'

if [ -d $path ]
then 
	echo 'directory'
else
	echo 'not directory'
fi
echo '----------------------------------------'
echo 'File operator -e'
if [ -e $path/hello ]
then
	echo 'file present'
else
	echo 'fine not found'
fi

echo '----------------------------------------'
echo 'File operator -f'
if [ -f $path/hi ]
then
	echo $path/hi is a file
else
	echo $path/hi is not a file
fi

echo '----------------------------------------'
echo 'File operator -r'
if [ -r $path/hi ]
then 
	echo $path/hi is readable
else
	echo $path/hi is not readable
fi

echo '----------------------------------------'
echo 'File operator -s'
if [ -s $path/empty ]
then
	echo $path/empty is non empty
else
	echo $path/empty is empty
fi

echo '----------------------------------------'
echo 'File operator -w'
if [ -w $path/hi ]
then
	echo 'Writable'
else
	echo 'No write permission'
fi

echo '----------------------------------------'
echo 'File operator -w'
if [ -x $path/hi ]
then
	echo 'Executable'
else
	echo 'Not executable' 
fi

echo '----------------------------------------'
if [ -O $path/hi ]
then
	echo 'Owner'
else
	echo 'Not owner'
fi

