#!/bin/bash
#Program to count total number of ordinary files, hidden files and directories and subdirectories

echo 'Total files present in `pwd` is ..'
find -type f | wc -l

echo 'Total hidden files in `pwd` is ..'
find -type f | grep '/\.' | wc -l

echo 'Total directories in `pwd` is ..'
var=`find -type d |wc -l`
var=`expr $var - 1`
echo $var 
