#!/bin/bash

read -p 'Enter number 1:' n1
read -p 'Enter number 2:' n2

if (( $n1 > $n2 ))
then
	echo $n1 is greater than $n2
	echo relative magnitude is $[$n1 - $n2]
else
	echo $n2 is greater than $n1
	echo relative magnitude is $[$n2 - $n1]
fi


