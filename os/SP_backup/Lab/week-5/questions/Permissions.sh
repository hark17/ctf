#!/bin/bash
#input file as parameter

file=$1
echo $file has..
if [ -e $file ]
then
	if [ -r $file ]
	then
		echo read permission
	fi

	if [ -w $file ]
	then
		echo write permission
	fi

	if [ -x $file ]
	then
		echo execute permission
	fi
else
	echo File not found
fi
