#!/bin/bash
echo 'Program to input a file name and display attributes and content'
file=$1

echo file name is $file
echo '----------------------------------------'

echo 'attributes of the files are '
ls -l $file
echo '----------------------------------------'

echo Content of the file $ file is
cat $file
