Date of creation : 23-Jul-2019

What is ULM?
	"The unified modeling language (UML) is a language of specifying, visualising, constructingand documenting the artifacts of software system, as well as for business modeling and other non software systems" - OMG UML Specifications.
Why use UML?
- UML has become the de-factor modeling for modeling object-oriented system.
- UML is extensible and method independent.
- UML is not perfect but good enough.
UML FigureTypes
There is several types of UML diagrams:
Use-case diagram
- Show actors, use-cases and relationship between them.
Class diagram
- Shows relation between class and pertinent information about classes themselves
Object diagram
- Shows configuration of an object at an instance of time.
Interaction diagram
- Shows an interaction between group of collaborating objects.
- Two types: Collaboration diagram and sequence diagram.
Package diagram
- Shows system structure at the library/package level.
State diagram
- Describes behavior of an instance of a class in terms of 



 

